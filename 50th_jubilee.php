<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
  <head>
      <?php include 'templates/head.php' ?>
  </head>
  <body>
      <?php include 'templates/header.php' ?>

    <div class="container py-2">
      <div class="row">
        <div class="col-lg-12 fadein">
          <h2>50th Jubilee</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 fadein fadein-1">
          <p>The PCAA is currently in the process of organising the Jubilee Celebration for the 50th Anniversary of the
            school. This promises to be an unforgettable event for everyone who is, and has been, associated with the
            school.</p>
        </div>
      </div>
        <?php
        $db = new SQLite3("database.db");
        $result = $db->query("SELECT * FROM \"50th_jubilee\" ORDER BY ID");
        $id = 1;
        while ($row = $result->fetchArray()) {
            echo '<div class="row mb-4 fadein fadein-' . ++$id . '">
                    <div class="col-xs-12 col-lg-6 px-2 event-left">
                      <img class="img-responsive rounded my-1" src="images/' . $row["image_path"] . '" alt="">
                    </div>
                    <div class="col-xs-12 col-lg-6 px-2 event-right">
                      <h3>' . $row["name"] . '</h3>
                      <p>Date: ' . $row["date"] . '</p>
                      <p>Organiser: ' . $row["organiser"] . '</p>
                      <p>Venue: ' . $row["venue"] . '</p>
                      <p class="event-details">' . str_replace("\n", "<br/>", $row["details"]) . '</p>
                      <h6>Do you want to attend this event?</h6>
                      <p id="contact" class="btn btn-info">Contact us</p>
                    </div>
                  </div>';
        }
        ?>
    </div>

      <?php include 'templates/footer.php' ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

      <?php include 'templates/fadeout.html' ?>
  </body>

</html>