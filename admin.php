<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
  <head>
    <link rel="stylesheet" href="node_modules/leaflet/dist/leaflet.css"/>

    <script type="text/javascript" src="node_modules/leaflet/dist/leaflet.js"></script>

      <?php include 'templates/head.php' ?>
  </head>
  <body>
      <?php include 'templates/header.php' ?>

    <div class="container py-2">
      <div class="row mb-3">
        <div class="col-xs-8 col-lg-11">
          <h2>Admin-Panel</h2>
        </div>
          <?php
          if (isset($_COOKIE['password'])) {
          $password = $_COOKIE['password'];
          if ($password == file_get_contents("password.txt")) { ?>
        <div class="col-xs-4 col-lg-1">
          <a href="logout" class="btn btn-danger" style="margin-right: 0; margin-left: auto;">Logout</a>
        </div>
      </div>
      <hr>

      <ul class="nav nav-pills">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#menu_jubilee">50th Jubilee</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu_committee">Committee</a>
        </li>
      </ul>

      <div class="tab-content">
        <div id="menu_jubilee" class="tab-pane active">
          <ul class="list-group">
              <?php
              function rowStringEvents($name, $date, $organiser, $venue, $image, $details)
              {
                  return '<li class="list-group-item">
                    <h6 class="event-name">' . $name . '</h6>
                    <div class="event-date">Date: ' . $date . ' <i title="Change date" class="fas fa-pencil-alt change-event-date"></i></div>
                    <div class="event-organiser">Organiser: ' . $organiser . ' <i title="Change organiser" class="fas fa-pencil-alt change-event-organiser"></i></div>
                    <div class="event-venue">Venue: ' . $venue . ' <i title="Change venue" class="fas fa-pencil-alt change-event-venue"></i></div>
                    <div class="event-image-path">Image-Path: ' . $image . ' <i title="Change image" class="fas fa-pencil-alt change-event-image-path"></i></div>
                    <div class="event-details">Details: ' . $details . ' <i title="Change details" class="fas fa-pencil-alt change-event-details"></i></div>
                    <i title="Delete" class="fas fa-trash delete-event"></i>
                   </li>';
              }

              $db = new SQLite3("database.db");
              $result = $db->query("SELECT * FROM '50th_jubilee' ORDER BY ID");
              while ($row = $result->fetchArray())
                  echo rowStringEvents($row["name"], $row["date"], $row["organiser"], $row["venue"], $row["image_path"], $row["details"]);
              ?>
            <li class="list-group-item">
              <i title="Add an event" class="fas fa-plus add-event"></i>
            </li>
          </ul>
        </div>
        <div id="menu_committee" class="tab-pane">
          <ul class="list-group">
            <li class="list-group-item">
              <div class="member-row">
                <div class="member-name"><b>Name</b></div>
                <div class="member-role"><b>Role</b></div>
                <div class="member-functions"><b>Functions</b></div>
              </div>
            </li>
              <?php
              function rowStringMembers($name, $role)
              {
                  return '<li class="list-group-item">
                    <div class="member-row">
                      <div class="member-name">' . $name . '</div>
                      <div class="member-role">' . $role . '</div>
                      <div class="member-functions">
                        <i title="Change role" class="fas fa-pencil-alt rename-member"></i>
                        <i title="Delete" class="fas fa-trash delete-member"></i>
                      </div>
                    </div>
                   </li>';
              }

              $db = new SQLite3("database.db");
              $result = $db->query("SELECT * FROM committee ORDER BY ID");
              while ($row = $result->fetchArray())
                  echo rowStringMembers($row["name"], $row["role"]);
              ?>
            <li class="list-group-item">
              <div class="member-row">
                <div class="member-functions">
                  <i title="Add a member" class="fas fa-plus add-member"></i>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
        <?php
        } else {
        $past = time() - 3600;
        setcookie("password", "", $past);
        include 'templates/passwordform.php';
        ?>
    </div>
  <?php
  }
  } else
      include 'templates/passwordform.php';

  if (isset($_POST['submit'])) {
      $password = $_POST['password'];
      if ($password == file_get_contents("password.txt")) {
          $hour = time() + (10 * 365 * 24 * 60 * 60);
          setcookie("password", $password, $hour);
          header('Location: admin');
          exit;
      } else
          echo 'Wrong password.';
  }
  ?>
    </div>

      <?php include 'templates/footer.php' ?>

    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript" src="node_modules/bootbox/bootbox.min.js"></script>

    <script type="text/javascript">
        const titleCase = (roleName) => {
            return roleName
                .split(' ')
                .map(word => word.charAt(0).toUpperCase() + word.substr(1, word.length).toLowerCase())
                .join(' ');
        };

        const rowStringEvents = (name, date, organiser, venue, image, details) => {
            return '<li class="list-group-item">\n<h6 class="event-name">' + name + '</h6>\n<div class="event-date">Date: ' + date +
                ' <i title="Change date" class="fas fa-pencil-alt change-event-date"></i></div>\n<div class="event-organiser">Organiser: ' + organiser +
                ' <i title="Change organiser" class="fas fa-pencil-alt change-event-organiser"></i></div>\n<div class="event-venue">Venue: ' + venue +
                ' <i title="Change venue" class="fas fa-pencil-alt change-event-venue"></i></div>\n<div class="event-image-path">Image-Path: ' + image +
                ' <i title="Change image" class="fas fa-pencil-alt change-event-image-path"></i></div>\n<div class="event-details">Details: ' + details +
                ' <i title="Change details" class="fas fa-pencil-alt change-event-details"></i></div>\n<i title="Delete" class="fas fa-trash delete-event"></i>\n</li>';
        };

        const rowStringMembers = (name, role) => {
            return '<li class="list-group-item">\n<div class="member-row">\n<div class="member-name">' + name +
                '</div>\n<div class="member-role">' + role +
                '</div>\n<div class="member-functions">\n<i title="Change role" class="fas fa-pencil-alt rename-member"></i>\n<i title="Delete" class="fas fa-trash delete-member"></i>\n</div>\n</div>\n</li>';
        };

        const askAddQuestions = (question, tableName, valueArray, answers, index, event) => {
            if (index === valueArray.length) {
                const answerJSON = {};
                for (let value of valueArray)
                    answerJSON[':' + value] = answers[value];
                $.post('executesql.php', {
                        query: "INSERT INTO " + tableName + " (" + valueArray.join(", ") + ") VALUES (" + valueArray.map(e => ":" + e).join(", ") + ")",
                        args: JSON.stringify(answerJSON)
                    }, () => {
                        const answersArray = [];
                        $.each(answers, (key, value) => answersArray.push(value));
                        $(event ? rowStringEvents(...answersArray) : rowStringMembers(...answersArray))
                            .insertBefore($("ul.list-group li.list-group-item:last-child"));
                    }
                );
                return;
            }

            const value = valueArray[index];
            bootbox.prompt({
                    title: question.replace("{value}", value.replace("_", " ")),
                    callback: (newValue) => {
                        if (newValue == null || newValue === "")
                            return;
                        answers[value] = newValue;
                        askAddQuestions(question, tableName, valueArray, answers, index + 1, event);
                    }
                }
            );
        };

        const askRenameQuestions = (question, value, tableName, valueName, name, callback) => {
            bootbox.prompt({
                    title: question,
                    value: value,
                    callback: (newValue) => {
                        if (newValue == null || newValue === "")
                            return;

                        if (valueName !== 'image_path' && valueName !== 'details')
                            newValue = titleCase(newValue);

                        $.post('executesql.php', {
                                query: "UPDATE " + tableName + " SET " + valueName + "=:newValue WHERE name=:name",
                                args: JSON.stringify({':newValue': newValue, ':name': name})
                            }, callback(newValue)
                        );
                    }
                }
            );
        };

        const confirmDelete = (question, tableName, name, removeElement) => {
            bootbox.confirm(question, result => {
                if (result === null || result === false)
                    return;

                $.post('executesql.php', {
                        query: "DELETE FROM " + tableName + " WHERE name=:name",
                        args: JSON.stringify({':name': name})
                    }, () => removeElement.remove()
                );
            });
        };

        $(document).on('click', 'div#menu_jubilee i.fas', function () {
            const groupItem = $(this).closest('li.list-group-item');

            if ($(this).hasClass('add-event')) {
                askAddQuestions(
                    "Please type in the {value} of the event.",
                    "'50th_jubilee'",
                    ["name", "date", "organiser", "venue", "image_path", "details"],
                    {},
                    0,
                    true
                );
            } else if ($("div[class^='change-event-'], div[class*=' change-event-']", this)) {
                const className = $.grep($(this).attr('class').split(/\s+/), (item) => item.startsWith('change-event-'))[0],
                    nameDiv = $(groupItem).find('h6.event-name'),
                    name = nameDiv.text().trim(),
                    valueName = className.replace('change-event-', ''),
                    valueName_ = valueName.replace('-', '_'),
                    parentDiv = $(this).closest('div');
                let value = $(parentDiv).text().trim();
                value = value.substr(value.indexOf(": ") + ": ".length);

                askRenameQuestions(
                    `Please type in ${name}'s new ${valueName}.`,
                    value,
                    "'50th_jubilee'",
                    valueName_,
                    name,
                    (newValue) => $(parentDiv).html($(parentDiv).html().replace(value, newValue))
                );
            } else if ($(this).hasClass('delete-event')) {
                const nameDiv = $(groupItem).find('h6.event-name'),
                    name = nameDiv.text().trim();
                confirmDelete(
                    `Are you sure you want to delete the event ${name}?`,
                    "'50th_jubilee'",
                    name,
                    $(groupItem)
                );
            }
        });

        $(document).on('click', 'div#menu_committee i.fas', function () {
            const row = $(this).closest('div.member-row');

            if ($(this).hasClass('add-member')) {
                askAddQuestions(
                    "Please type in the member's {value}.",
                    "'committee'",
                    ["name", "role"],
                    {},
                    0,
                    false
                );
                return;
            }

            const name = $(row).find('div.member-name').html(),
                roleElement = $(row).find('div.member-role'),
                role = $(roleElement).html();

            if ($(this).hasClass('rename-member')) {
                askRenameQuestions(
                    `Please type in ${name}'s new role.`,
                    role,
                    "committee",
                    "role",
                    name,
                    (newValue) => roleElement.html(newValue)
                );
            } else if ($(this).hasClass('delete-member')) {
                confirmDelete(
                    `Are you sure you want to delete the ${role} ${name}?`,
                    "committee",
                    name,
                    $(row).closest("li.list-group-item")
                );
            }
        });
    </script>

      <?php include 'templates/fadeout.html' ?>
  </body>
</html>