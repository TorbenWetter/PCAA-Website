<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
  <head>
      <?php include 'templates/head.php' ?>
  </head>
  <body>
      <?php include 'templates/header.php' ?>

    <div class="container py-2">
      <div class="row">
        <div class="col-lg-12 fadein">
          <h2>The Pōhutukawa College Alumni Association</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <p class="fadein fadein-1">The association is managed by the PCAA Executive Committee.<br/>
            To become a member of the PCAA, email us from the contact page.</p>
            <p id="contact" class="btn btn-info fadein fadein-2">Visit the Contact page</p>
          <h6 class="fadein fadein-3">These are the current members:</h6>
        </div>
      </div>
      <div class="row">
          <?php
          $heads = '<ul class="list-group-horizontal col-xs-12 col-sm-6 fadein fadein-4">';
          $members = '<ul class="list-group-horizontal col-xs-12 col-sm-6 fadein fadein-5">';

          $db = new SQLite3("database.db");
          $result = $db->query("SELECT * FROM committee ORDER BY ID");
          while ($row = $result->fetchArray()) {
              $role = $row['role'];
              if ($role !== 'Member')
                  $heads .= '<li class="list-group-item">
                    <h5 class="mb-0">' . $row['name'] . '</h5>
                    <span class="text-muted" style="font-size: small;">' . $role . '</span>
                  </li>';
              else
                  $members .= '<li class="list-group-item">
                    <h5 class="mb-0">' . $row['name'] . '</h5>
                    <span class="text-muted" style="font-size: small;">Committee Member</span>
                  </li>';
          }
          echo $heads . '</ul>' . $members . '</ul>';
          ?>
      </div>
    </div>

      <?php include 'templates/footer.php' ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

      <?php include 'templates/fadeout.html' ?>
  </body>

</html>