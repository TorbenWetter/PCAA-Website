<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
  <head>
    <link rel="stylesheet" href="node_modules/leaflet/dist/leaflet.css"/>

    <script type="text/javascript" src="node_modules/leaflet/dist/leaflet.js"></script>

      <?php include 'templates/head.php' ?>
  </head>
  <body>
      <?php include 'templates/header.php' ?>

    <div class="container py-2">
      <div class="row">
        <div class="col-lg-12 fadein">
          <h2>Contact the Alumni Association</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-lg-6 mb-4 fadein fadein-1">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Address details for PCAA</h5>
              <p class="card-text card-text-contact card-address">
                The Pōhutukawa College Alumni Society<br/>
                PO Box 2569<br/>
                Rickville 0798
              </p>
              <p class="card-text card-text-contact card-phone">
                Phone: 0938 659 194
              </p>
              <p class="card-text card-text-contact card-mail">
                Email: pc.alumnisociety@pc.college.tk
              </p>
            </div>
            <div class="card-footer">
              <a href="mailto:pc.alumnisociety@pc.college.tk" class="btn btn-info">Write an email</a>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-lg-6 mb-4 fadein fadein-2">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Address details for Pōhutukawa College</h5>
              <p class="card-text card-text-contact card-address">
                Pōhutukawa College<br/>
                1253 Grey Drive<br/>
                Rickville 0798
              </p>
              <p class="card-text card-text-contact card-phone">
                Phone: 0938 659 193
              </p>
              <p class="card-text card-text-contact card-mail">
                Email: admin@pc.college.tk
              </p>
            </div>
            <div class="card-footer">
              <a href="mailto:admin@pc.college.tk" class="btn btn-info">Write an email</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 px-0 fadein fadein-3">
        <div class="row">
          <div class="col-lg-12">
            <h4>Find us on the map</h4>
          </div>
        </div>
        <div class="row mb-2">
          <div class="col-lg-12">
            <div id="map"></div>
          </div>
        </div>
      </div>
    </div>

      <?php include 'templates/footer.php' ?>

    <script type="text/javascript">
        var map = L.map('map', {
            minZoom: 14,
            maxZoom: 16,
            zoomSnap: 0.05
        }).setView([-36.874682, 174.8026543], 14);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([-36.87502, 174.7971268]).addTo(map)
            .bindPopup('<b>The Pōhutukawa College<br/>Alumni Society</b><br/>PO Box 2569<br/>Rickville 0798', {
                autoClose: false
            }).openPopup();

        L.marker([-36.876893, 174.8058928]).addTo(map)
            .bindPopup('<b>Pōhutukawa College</b><br/>1253 Grey Drive<br/>Rickville 0798', {
                autoClose: false
            }).openPopup();

        setTimeout(function () {
            map.flyTo([-36.874682, 174.8026543], 16, {
                duration: 2.0
            });
        }, 500);
    </script>

    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

      <?php include 'templates/fadeout.html' ?>
  </body>
</html>