<?php
if (isset($_COOKIE['password'])) {
    $password = $_COOKIE['password'];
    if ($password == file_get_contents("password.txt")) {
        $db = new SQLite3("database.db");
        $stmt = $db->prepare($_POST['query']);

        $args = json_decode($_POST['args']);
        foreach ($args as $key => $value)
            $stmt->bindValue($key, $value, SQLITE3_TEXT);

        $stmt->execute();
    }
}