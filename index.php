<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
  <head>
      <?php include 'templates/head.php' ?>
  </head>
  <body>
      <?php include 'templates/header.php' ?>

    <div class="container py-4">
      <div class="row">
        <div class="col-lg-12">
          <img class="img-responsive rounded fadein" src="images/banner.jpg" alt="Banner Image" style="width: 100%;">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 mt-2 fadein fadein-1">
          <h2>Pōhutukawa College Alumni Association</h2>
          <p>The Pōhutukawa College Alumni Association (PCAA) is a group of former students, staff, parents and other
            members of the community that have been involved with Pōhutukawa College over the years.</p>
          <p>The association serves a number of purposes including:</p>
          <ul>
            <li>helping former students, staff and people associated with the school to keep in touch</li>
            <li>organising social events and meetings for alumni</li>
            <li>publishing newsletters and magazines to keep alumni up-to-date on important school events/information
            </li>
            <li>raising funds for the college</li>
          </ul>
          <p>The PCAA is currently in the process of organising the Jubilee Celebration for the 50th Anniversary of the
            school.</p>
        </div>
        <div class="col-lg-4 mt-3 fadein fadein-2">
          <h3>Upcoming Events</h3>
          <div class="card">
            <div id="event-overview" class="card-body">
              <h6 class="card-title">Pōhutukawa College<br/>50th Jubilee Celebrations</h6>
                <?php
                $db = new SQLite3("database.db");
                $result = $db->query("SELECT * FROM \"50th_jubilee\" ORDER BY RANDOM()");
                for ($i = 0; $i < 3; $i++) {
                    $row = $result->fetchArray();
                    if ($i !== 0)
                        echo '<hr/>';
                    echo '<div class="row fadein fadein-' . (3 + $i) . '">
                      <div class="col-lg-3 pl-3 pr-1">
                        <img class="img-responsive rounded event-img" src="images/' . $row["image_path"] . '" alt="">
                      </div>
                      <div class="col-lg-9 event-details">
                        <p class="card-text">' . $row["details"] . '</p>
                      </div>
                    </div>';
                }
                ?>
            </div>
            <div class="card-footer">
              <p id="50th_jubilee" class="btn btn-info">Visit the Jubilee page</p>
            </div>
          </div>
        </div>
      </div>
    </div>

      <?php include 'templates/footer.php' ?>

    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

      <?php include 'templates/fadeout.html' ?>
  </body>
</html>
