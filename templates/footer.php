<footer class="bg-dark footer">
  <p class="m-0 text-center text-white">Copyright &copy; Torben Wetter <?php echo date("Y"); ?> | <a href="admin" class="admin-link">Admin-Panel</a></p>
</footer>