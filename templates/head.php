<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Website of the Pōhutukawa College Alumni Association">
<meta name="author" content="Torben Wetter">

<title><?php
    $filename = basename($_SERVER['PHP_SELF']);
    if ($filename == "index.php")
        echo "Pōhutukawa College Alumni Association";
    else {
        $page = explode(".", $filename)[0];
        $name = ucwords(str_replace("_", " ", $page));
        echo "PCAA | " . $name;
    }
    ?></title>

<link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
      integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="css/stylesheet.css" rel="stylesheet">