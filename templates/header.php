<?php $filename = basename($_SERVER['PHP_SELF']); ?>
<nav
    class="navbar navbar-dark navbar-expand-lg fixed-top<?php echo explode(".", $filename)[0] != 'admin' ? " fadedown" : "" ?>">
  <div class="container">
    <a class="navbar-brand" id="long_name" href="">Pōhutukawa College Alumni Association</a>
    <a class="navbar-brand" id="short_name" href="">PCAA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item<?php echo explode(".", $filename)[0] == 'index' ? " active" : "" ?>">
          <p class="nav-link" id="home">Home</p>
        </li>
        <li class="nav-item<?php echo explode(".", $filename)[0] == "50th_jubilee" ? " active" : "" ?>">
          <p class="nav-link" id="50th_jubilee">50th Jubilee</p>
        </li>
        <li class="nav-item<?php echo explode(".", $filename)[0] == "committee" ? " active" : "" ?>">
          <p class="nav-link" id="committee">Committee</p>
        </li>
        <li class="nav-item<?php echo explode(".", $filename)[0] == "contact" ? " active" : "" ?>">
          <p class="nav-link" id="contact">Contact</p>
        </li>
      </ul>
    </div>
  </div>
</nav>