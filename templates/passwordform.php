<form action="admin" method="post">
  <div class="form-group" id="admin-form">
    <label for="password" class="mt-2">Please type in the password to login.</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
  </div>
  <button type="submit" name="submit" class="btn btn-primary btn-flat m-b-30" id="admin-login-button">Log in</button>
</form>